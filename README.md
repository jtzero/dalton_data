database for [Dalton](https://gitlab.com/betapig/dalton)

### SECRETS:
  DALTON_DB_PASSWORD: regular user for the dalton database
  DALTON_POSTGRES_USER: super user namespaced version of `POSTGRES_USER`
  DALTON_POSTGRES_PASSWORD: super user namespaced version of `POSTGRES_PASSWORD`
 
for more info see https://hub.docker.com/_/postgres/

### TODO:
  - remove the v infront of tags to match SEMVER 2.0.0
