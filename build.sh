#!/bin/bash

default="registry.gitlab.copm/betapig/dalton_data"
docker build -t "${1:-$default}" ./
