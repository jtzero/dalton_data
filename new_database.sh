#!/bin/bash
set -e

PGUSER=`cat /run/secrets/DALTON_POSTGRES_USER`
PGPASSWORD=`cat /run/secrets/DALTON_POSTGRES_PASSWORD`

# TODO is needed?
if psql -U "${DALTON_PGUSER}" template1 -lqt | cut -d \| -f 1 | grep -qw dalton; then
echo "host all  all    0.0.0.0/0  md5" >> /var/lib/postgresql/data/pg_hba.conf

DALTON_DB_PASSWORD=`cat /run/secrets/DALTON_DB_PASSWORD`

psql -U "${DALTON_PGUSER}" dalton <<- EOSQL
  CREATE USER dalton WITH PASSWORD '${DALTON_DB_PASSWORD}';
  GRANT ALL PRIVILEGES ON DATABASE dalton TO dalton;
EOSQL
fi

