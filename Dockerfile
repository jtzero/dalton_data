FROM library/postgres
ENV POSTGRES_DB dalton
COPY new_database.sh /docker-entrypoint-initdb.d/
